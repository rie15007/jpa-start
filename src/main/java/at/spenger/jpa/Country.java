package at.spenger.jpa;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Set;
import java.util.UUID;


/**
 * The persistent class for the country database table.
 * 
 */
@Entity
@NamedQuery(name="Country.findAll", query="SELECT c FROM Country c")
public class Country implements Serializable {
	@Override
	public String toString() {
		return "Country [code=" + code + ", continent=" + continent + ", gnp="
				+ gnp + ", GNPOld=" + GNPOld + ", governmentForm="
				+ governmentForm + ", headOfState=" + headOfState + "]";
	}
	private static final long serialVersionUID = 1L;

	@Id
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	// Cannot be used with String type!
	// The default is assigned generator which means the value of identifier has to be set by the application.
	@Column(name="CODE")
	private String code;
	
	@PrePersist
	private void ensureId()
	{
		setCode(UUID.randomUUID().toString().substring(0, 3));	
	}
	@Column(name="Code2")
	private String code2;
	@Column(name="Continent")
	private String continent;
	@Column(name="GNP")
	private Float gnp;
	@Column(name="GNPOld")
	private Float GNPOld;

	@Column(name="GOVERNMENT_FORM")
	private String governmentForm;
	@Column(name="head_of_state")
	private String headOfState;

//	
//	@Column(name="IndepYear")
//	private short indepYear;
//	@Column(name="LifeExpectancy")
//	private float lifeExpectancy;
//	@Column(name="LocalName")
//	private String localName;
//	@Column(name="Name")
//	private String name;
//	@Column(name="Population")
//	private int population;
//	@Column(name="Region")
//	private String region;
//	@Column(name="SurfaceArea")
//	private float surfaceArea;


	public Country() {
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCode2() {
		return this.code2;
	}

	public void setCode2(String code2) {
		this.code2 = code2;
	}

	public String getContinent() {
		return this.continent;
	}

	public void setContinent(String continent) {
		this.continent = continent;
	}

	public Float getGnp() {
		return this.gnp;
	}

	public void setGnp(Float gnp) {
		this.gnp = gnp;
	}

	public Float getGNPOld() {
		return this.GNPOld;
	}

	public void setGNPOld(Float GNPOld) {
		this.GNPOld = GNPOld;
	}

	public String getGovernmentForm() {
		return this.governmentForm;
	}

	public void setGovernmentForm(String governmentForm) {
		this.governmentForm = governmentForm;
	}

	public String getHeadOfState() {
		return this.headOfState;
	}

	public void setHeadOfState(String headOfState) {
		this.headOfState = headOfState;
	}

//	public short getIndepYear() {
//		return this.indepYear;
//	}
//
//	public void setIndepYear(short indepYear) {
//		this.indepYear = indepYear;
//	}
//
//	public float getLifeExpectancy() {
//		return this.lifeExpectancy;
//	}
//
//	public void setLifeExpectancy(float lifeExpectancy) {
//		this.lifeExpectancy = lifeExpectancy;
//	}
//
//	public String getLocalName() {
//		return this.localName;
//	}
//
//	public void setLocalName(String localName) {
//		this.localName = localName;
//	}
//
//	public String getName() {
//		return this.name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public int getPopulation() {
//		return this.population;
//	}
//
//	public void setPopulation(int population) {
//		this.population = population;
//	}
//
	
//	
//	public String getRegion() {
//		return this.region;
//	}
//
//	public void setRegion(String region) {
//		this.region = region;
//	}
//
//	public float getSurfaceArea() {
//		return this.surfaceArea;
//	}
//
//	public void setSurfaceArea(float surfaceArea) {
//		this.surfaceArea = surfaceArea;
//	}

	//bi-directional many-to-one association to City
	@OneToMany(mappedBy="country1", 
			orphanRemoval=true, // wenn City aus Liste entfernt wird dann auch aus DB entfernen
			cascade=CascadeType.ALL // transitive Persistenz, 
			)
	private Set<City> cities;
	
	public Set<City> getCities() {
		return this.cities;
	}

	public void setCities(Set<City> cities) {
		this.cities = cities;
	}


	public City addCity(City city) {
		getCities().add(city);
		city.setCountry1(this);

		return city;
	}

	public City removeCity(City city) {
		getCities().remove(city);
		city.setCountry1(null);

		return city;
	}


	@OneToOne
	@JoinColumn(name = "capital", nullable=true)
		// capital ist Fremdschluessel in die Tabelle Country
		// indicates that this entity is the owner of the relationship
	private City city2;


	public City getCapital() {
		return this.city2;
	}

	public void setCabital(City city) {
		this.city2 = city;
	}
	
	

}
