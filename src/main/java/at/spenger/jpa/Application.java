package at.spenger.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application implements CommandLineRunner
{
	@Autowired
	private CityRepository cityRepository;
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
	@Override
	public void run(String... arg0) throws Exception 
	{
		for(City c : cityRepository.findAll())
		{
			System.out.println(c);
		}
		
		System.out.println("--------------------------------------------------------");
			
		//System.out.println(cityRepository.count());
		
		System.out.println("--------------------------------------------------------");
			
		System.out.println(cityRepository.findOne(1));
		
		System.out.println("--------------------------------------------------------");
		
		for(City c : cityRepository.findByDistrict("California"))
		{
			System.out.println(c);
		}
		
		System.out.println("--------------------------------------------------------");
		
		System.out.println(cityRepository.count());
		
		System.out.println("--------------------------------------------------------");
		
			cityRepository.delete(4000);

		System.out.println(cityRepository.count());
		
		System.out.println("--------------------------------------------------------");
		
		boolean b1 = cityRepository.exists((int) cityRepository.count());
		boolean b2 = cityRepository.exists((int) cityRepository.count() * 100);
		System.out.println(b1);
		
		System.out.println("--------------------------------------------------------");
		
		System.out.println(b2);
		
		System.out.println("--------------------------------------------------------");
	}
}
