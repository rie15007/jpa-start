package at.spenger.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.springframework.data.jpa.domain.AbstractPersistable;

/**
 * The persistent class for the city database table.
 * 
 */
@Entity
public class City extends AbstractPersistable<Integer> {
	private static final long serialVersionUID = 1L;

	// @Column(name = "COUNTRY_CODE", length = 3)
	// private String countrycode;
	//
	// public String getCountryCode() {
	// return this.countrycode;
	// }
	//
	// public void setCountryCode(String countrycode) {
	// this.countrycode = countrycode;
	// }

	@Column(name = "District")
	private String district;

	@Override
	public String toString() {
		return "City: id=" + getId() + ", district=" + district + ", name="
				+ name + ", population=" + population + ", country="
				+ (country1 != null ? country1.getContinent() : "NIX")
				+ getCountry2();
	}

	private String name;

	private int population;

	public City() {
	}

	public String getDistrict() 
	{
		return this.district;
	}

	public void setDistrict(String district) 
	{
		this.district = district;
	}

	public String getName() 
	{
		return this.name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public int getPopulation() 
	{
		return this.population;
	}

	public void setPopulation(int population) 
	{
		this.population = population;
	}

	@ManyToOne
	@JoinColumn(name = "COUNTRY_CODE", nullable = false)
	// COUNTRY_CODE ist Fremdschluessel in die Tabelle City
	// indicates that this entity is the owner of the relationship
	private Country country1;

	public Country getCountry1() 
	{
		return this.country1;
	}

	public void setCountry1(Country country1) 
	{
		this.country1 = country1;
	}

	@OneToOne(mappedBy = "city2")
	private Country country2;

	public Country getCountry2() {
		return this.country2;
	}

	public void setCountry2(Country country2) {
		this.country2 = country2;
	}

}