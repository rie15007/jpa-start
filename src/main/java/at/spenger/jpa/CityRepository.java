package at.spenger.jpa;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.lang.String;

import at.spenger.jpa.City;
import at.spenger.jpa.Country;	

public interface CityRepository extends CrudRepository<City, Integer>
{
    List<City> findAll();    
    
    @Override
    public City findOne(Integer arg0);
    
    List<City> findByName(String name);
    
    @Override
    public long count();
    
    List<City> findByDistrict(String district);
    
    @Override
    public void delete(Integer arg0);
    
    @Override
    public boolean exists(Integer arg0);
    
}
